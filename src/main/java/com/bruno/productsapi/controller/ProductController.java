package com.bruno.productsapi.controller;

import com.bruno.productsapi.configurations.annotations.*;
import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.ShippingRequest;
import com.bruno.productsapi.model.entity.ProductQuery;
import com.bruno.productsapi.model.mapper.request.ProductRequest;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import com.bruno.productsapi.service.ProductServiceFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;


// The Controller Class contains the REST Endpoints for the Application
// It is the initial point for all requests
@RestController // This annotation marks this class as a Rest Controller
@RequestMapping("v1/products") // This annotation marks this class as an endpoint for the API
@RequiredArgsConstructor // Lombok constructor for the dependency injections
@Api(value = "Products API") // Swagger Annotation for the API title
public class ProductController {

    // Dependency injection
    private final ProductServiceFacade productServiceFacade;

    // Route to calculate shipping passing a Product ID and a destination CEP
    @SwaggerShippingInit
    @GetMapping("/shipping/{id}")
    public List<ShippingEntity> calculateShipping(
            @RequestBody @Valid @ApiParam(name = "Shipping Request Body", value = "a JSON representing the Origin CEP Code " +
                    "and the Destination CEP Code.") ShippingRequest shippingRequest,
            @PathVariable @ApiParam(name = "Product ID", value = "The ID representing the product") String id) {
        return productServiceFacade.calculateShipping(shippingRequest, id);
    }

    // Route to Get One or more Products from the Database
    @SwaggerGetAllInit
    @GetMapping("") // Indicates this endpoint as a GET Http Method
    public List<ProductResponse> getProducts(
            @RequestParam(value = "ids", required = false, defaultValue = "")
            @ApiParam(name = "ids", value = "List of Product ID's", example = "62f0f6b3973cb841c7817ab1") List<String> ids) {
        return productServiceFacade.getProducts(ids);
    }

    // Route to get a Pageable Object for querying and display control.
    @SwaggerGetPageableInit
    @GetMapping("/pageable") // Indicates this endpoint as a GET Http Method
    // Needs Swagger to @ApiIgnore the Pageable, otherwise it displays the wrong parameters
    public Page<ProductResponse> getProductsPageable(@ApiIgnore() Pageable pageable) {
        return productServiceFacade.getProductsPageable(pageable);
    }

    @SwaggerQueryInit
    @GetMapping("/query")
    public List<ProductResponse> findProducts(@RequestBody @ApiParam(name = "Product Query Body", value = "a JSON representing the product, " +
            "with name, category, and price range.") ProductQuery productToFind) {
        return productServiceFacade.findProducts(productToFind);
    }

    // Route to Add a Product to the Database
    @SwaggerPostInit
    @PostMapping("") // Indicates this endpoint as a POST Http Method
    @ResponseStatus(HttpStatus.CREATED) // Indicates the Http Status Code for the response
    public ProductResponse addProduct(@RequestBody @Valid @ApiParam(name = "Product Body", value = "a JSON representing the product, " +
            "with name, category, price and dimensions.") ProductRequest product) {
        return productServiceFacade.addProduct(product);
    }

    // Route to Update a Product in the Database
    @SwaggerPutInit
    @PutMapping("/{id}") // Indicates this endpoint as a PUT Http Method
    @ResponseStatus(HttpStatus.CREATED) // Indicates the Http Status Code for the response
    public ProductResponse updateProduct(
            @RequestBody @Valid @ApiParam(name = "Product Body", value = "a JSON representing the product, " +
                    "with name, category, price and dimensions.") ProductRequest productRequest,
            @PathVariable @ApiParam(name = "id", value = "Product ID to be updated.", example = "62f0f6b3973cb841c7817ab1")
            String id) {
        return productServiceFacade.updateProduct(productRequest, id);
    }

    // Route to Delete One or More Products from the Database
    @SwaggerDeleteInit
    @DeleteMapping("") // Indicates this endpoint as a DELETE Http Method
    @ResponseStatus(HttpStatus.NO_CONTENT) // Indicates the Http Status Code for the response
    public void deleteProducts(
            @RequestParam(required = false, defaultValue = "") @ApiParam(name = "id", value = "One or more product ID's " +
                    "to be deleted.", example = "62f0f6b3973cb841c7817ab1") List<String> ids,
            @RequestParam(required = false, defaultValue = "false") @ApiParam(name = "deleteAll", value = "Flag to confirm" +
                    " deletion of entire database.", example = "false") Boolean deleteAll) {
        productServiceFacade.deleteProducts(ids, deleteAll);
    }

}
