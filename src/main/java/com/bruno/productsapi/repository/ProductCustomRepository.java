package com.bruno.productsapi.repository;

import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.model.entity.ProductQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCustomRepository {

    List<ProductEntity> findProducts(ProductQuery productQuery);

}
