package com.bruno.productsapi.repository;

import com.bruno.productsapi.model.entity.Categories;
import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.model.entity.ProductQuery;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductCustomRepositoryImpl implements ProductCustomRepository{

    @Autowired
    MongoTemplate mongoTemplate;


    @Override
    public List<ProductEntity> findProducts(ProductQuery productQuery) {

        Query query = new Query();

    if (productQuery.getProductCategory().equals(Categories.ANY)) {
        query.addCriteria(
                Criteria.where("productName").regex(StringUtils.capitalize(productQuery.getProductName()))
                        .and("productPrice").gt(productQuery.getPriceFloor()).lt(productQuery.getPriceCeil()));
        return mongoTemplate.find(query, ProductEntity.class);
    } query.addCriteria(
                Criteria.where("productName").regex(StringUtils.capitalize(productQuery.getProductName()))
                        .and("productPrice").gt(productQuery.getPriceFloor()).lt(productQuery.getPriceCeil())
                        .and("productCategory").is(productQuery.getProductCategory()));

        return mongoTemplate.find(query, ProductEntity.class);
    }
}
