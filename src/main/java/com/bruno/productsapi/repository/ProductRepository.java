package com.bruno.productsapi.repository;

import com.bruno.productsapi.model.entity.ProductEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductRepository extends MongoRepository<ProductEntity, String>, ProductCustomRepository {



}

