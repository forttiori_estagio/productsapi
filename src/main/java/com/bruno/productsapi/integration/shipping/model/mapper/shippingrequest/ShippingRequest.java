package com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

// Class used for sending requests to the Integration ENDPOINT - produces a JSON Object to be used in the body
// of the request

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShippingRequest {

    @Valid
    From from;
    @Valid
    To to;
    @JsonIgnore
    Package packageInfo;

}


