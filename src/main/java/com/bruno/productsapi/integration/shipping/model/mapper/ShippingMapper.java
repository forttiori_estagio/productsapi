package com.bruno.productsapi.integration.shipping.model.mapper;

import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.ShippingResponse;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ShippingMapper {

    // This method receives a Shipping Response and returns a Shipping Entity
    public static ShippingEntity shippingResponseToShippingEntity(ShippingResponse shippingResponse) {

        return ShippingEntity.builder()
                .company(shippingResponse.getCompany().getName())
                .shippingPrice(Double.valueOf(shippingResponse.getPrice()))
                .deliveryName(shippingResponse.getName())
                .deliveryTime(shippingResponse.getDeliveryTime())
                .build();
    }

}
