package com.bruno.productsapi.integration.shipping;

import com.bruno.productsapi.exceptions.customexceptions.EntityNotFoundException;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.ShippingRequest;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.ShippingResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Component
public class ShippingClient {

    @Autowired
    @Qualifier("restTemplateMelhorEnvio")
    RestTemplate restTemplateMelhorEnvio;

    public List<ShippingResponse> fetchShippingData(ShippingRequest shippingRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<ShippingRequest> entity = new HttpEntity<>(shippingRequest, headers);

        List<ShippingResponse> shippingResponses = List.of(restTemplateMelhorEnvio.postForObject(
                "/api/v2/me/shipment/calculate", entity, ShippingResponse[].class));


        return shippingResponses;

    }
}