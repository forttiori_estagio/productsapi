package com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class From {

    @JsonProperty("postal_code")
    @Size(min = 8, max = 8, message = "The Origin CEP has to be in the following format: 00000000")
    String postalCode;
}
