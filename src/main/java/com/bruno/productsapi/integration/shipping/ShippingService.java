package com.bruno.productsapi.integration.shipping;

import com.bruno.productsapi.exceptions.customexceptions.EntityNotFoundException;
import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.model.mapper.ShippingMapper;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.Package;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.ShippingRequest;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.ShippingResponse;
import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ShippingService {

    private final ProductRepository productRepository;
    private final ShippingClient shippingClient;

    // Logic for calculating Shipping value, delivery time and company
    // By passing a product ID, get the Product Entity from the DB
    // Update the Shipping Request object to send it to the Rest Template Client
    public List<ShippingEntity> calculateShipping(ShippingRequest shippingRequest, String id) {
        ProductEntity productEntity = productRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("ID not found = " + id));
        // Update the Package Information with the Entity Returned from DB
        shippingRequest.setPackageInfo(
                Package.builder()
                        .height(productEntity.getProductHeight())
                        .length(productEntity.getProductLength())
                        .width(productEntity.getProductWidth())
                        .weight(productEntity.getProductWeight())
                        .build());
        List<ShippingResponse> shippingResponses = shippingClient.fetchShippingData(shippingRequest);

        // Filter the results and map it to a Shipping Entity object
        List<ShippingEntity> shippingEntities = shippingResponses.stream()
                .filter(shippingResponse -> shippingResponse.getError() == null)
                .map(ShippingMapper::shippingResponseToShippingEntity)
                .toList();
        // If nothing is returned, then an Exception has occurred
        if (shippingEntities.isEmpty()) {
            throw new EntityNotFoundException(shippingResponses.get(0).getError());
        }
        return shippingEntities;

    }
}
