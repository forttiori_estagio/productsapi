package com.bruno.productsapi.integration.mercadolivrewebscrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class MercadoLivreWebScrapperClient {

    @Autowired
    @Qualifier("restTemplateMercadoLivreWebScrapper")
    RestTemplate restTemplateMercadoLivreWebScrapper;

    public String fetchPriceData(String productName) {

        return restTemplateMercadoLivreWebScrapper.getForObject("/v1/webscrapper/" + productName, String.class);
    }
}
