package com.bruno.productsapi.integration.mercadolivrewebscrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MercadoLivreWebScrapperService {

    @Autowired
    MercadoLivreWebScrapperClient mercadoLivreWebScrapperClient;

    // Call the Client for price fetching

    public Double fetchProductPrice(String productName) {
        String priceReturned = mercadoLivreWebScrapperClient.fetchPriceData(productName);

        return Double.valueOf(priceReturned.replace(",", "."));
    }


}
