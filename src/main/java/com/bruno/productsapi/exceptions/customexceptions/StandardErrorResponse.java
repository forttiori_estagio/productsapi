package com.bruno.productsapi.exceptions.customexceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StandardErrorResponse {

    String errorName;
    Instant timestamp;
    List<StandardErrorObject> errorObjects;

}
