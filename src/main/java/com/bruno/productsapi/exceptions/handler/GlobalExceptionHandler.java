package com.bruno.productsapi.exceptions.handler;

import com.bruno.productsapi.exceptions.customexceptions.EntityNotFoundException;
import com.bruno.productsapi.exceptions.customexceptions.StandardErrorObject;
import com.bruno.productsapi.exceptions.customexceptions.StandardErrorResponse;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Instant;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(code = NOT_FOUND)
    public StandardErrorResponse handleNotFoundException(EntityNotFoundException e) {
        return (StandardErrorResponse.builder()
                .errorName("Not Found Exception")
                .timestamp(Instant.now())
                .errorObjects(
                        List.of(
                                StandardErrorObject.builder()
                                        .errorMessage(NOT_FOUND.name())
                                        .errorField((e.getMessage()))
                                        .build()))
                .build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = BAD_REQUEST)
    public StandardErrorResponse handleMethodArgumentNotValid(MethodArgumentNotValidException e) {
        List<FieldError> errorList = e.getBindingResult().getFieldErrors();
        return (StandardErrorResponse.builder()
                .errorName("Validation Exception")
                .timestamp(Instant.now())
                .errorObjects(errorList.stream()
                        .map(fieldError ->
                                StandardErrorObject.builder()
                                        .errorField(fieldError.getField())
                                        .errorMessage(fieldError.getDefaultMessage())
                                        .build())
                        .toList())
                .build());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(code = BAD_REQUEST)
    public StandardErrorResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        return StandardErrorResponse.builder()
                .errorName("Malformed Request")
                .timestamp(Instant.now())
                .errorObjects(
                        List.of(
                                StandardErrorObject.builder()
                                        .errorMessage(BAD_REQUEST.name())
                                        .errorField(e.getMessage())
                                        .build()))
                .build();
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(code = METHOD_NOT_ALLOWED)
    public StandardErrorResponse handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {

        return StandardErrorResponse.builder()
                .errorName("Not Allowed")
                .timestamp(Instant.now())
                .errorObjects(
                        List.of(
                                StandardErrorObject.builder()
                                        .errorMessage(METHOD_NOT_ALLOWED.name())
                                        .errorField(e.getMessage())
                                        .build()))
                .build();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(code = BAD_REQUEST)
    public StandardErrorResponse handleIllegalArgumentException(IllegalArgumentException e) {
        return StandardErrorResponse.builder()
                .errorName("Not Found")
                .timestamp(Instant.now())
                .errorObjects(
                        List.of(
                                StandardErrorObject.builder()
                                        .errorMessage(BAD_REQUEST.name())
                                        .errorField(e.getMessage())
                                        .build()
                        )).build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(code = INTERNAL_SERVER_ERROR)
    public StandardErrorResponse handleGenericException(Exception e) {
        return StandardErrorResponse.builder()
                .errorName("Something's Wrong")
                .timestamp(Instant.now())
                .errorObjects(
                        List.of(
                                StandardErrorObject.builder()
                                        .errorMessage(INTERNAL_SERVER_ERROR.name())
                                        .errorField(e.getMessage())
                                        .build()
                        )).build();
    }
}
