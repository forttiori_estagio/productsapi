package com.bruno.productsapi.service;

import com.bruno.productsapi.exceptions.customexceptions.EntityNotFoundException;
import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.model.entity.ProductQuery;
import com.bruno.productsapi.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// The Service Class implements the logic behind every request made to the Controller
// It stands between the Controller and the Repository

@Service // This annotation marks this class as a Service
@RequiredArgsConstructor
public class ProductService {

    // Dependency injection
    private final ProductRepository productRepository;

    // Method that to Get All Products or Products By ID
    public List<ProductEntity> getProducts(List<String> ids) {
        if (ids.isEmpty()) {
            return productRepository.findAll();
        }
        List<ProductEntity> responses = new ArrayList<>();
        productRepository.findAllById(ids).iterator().forEachRemaining(responses::add);
        if (responses.isEmpty()) {
            throw new EntityNotFoundException("No Entities Found");
        }
        return responses;
    }

    // Logic for GET request to generate a Pageable response
    public Page<ProductEntity> getProductsPageable(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    // Logic for the POST request to add a product to the database
    public ProductEntity addProduct(ProductEntity product) {
        // Save the Entity in the Database and returns it
        return productRepository.save(product);
    }

    // Logic for the PUT request to update some or all product attributes in the Database
    public ProductEntity updateProduct(ProductEntity product, String id) {
        // Check to see if the ID is valid first
        ProductEntity productEntitySaved = productRepository.findById(id).orElseThrow(() -> // Mongo method to find and return an item by ID
                new EntityNotFoundException("ID not found = " + id));

        product.setId(productEntitySaved.getId()); // Get ID from DB and set it in new Product

        return productRepository.save(product);
    }

    // Logic for the DELETE request to remove one or more products with given IDs from the Database.
    // If no ID is passed it deletes the entire Database
    public void deleteProducts(List<String> ids, Boolean deleteAll) {
        if (deleteAll) {
            productRepository.deleteAll();
        } else if (ids.isEmpty()) {
            throw new EntityNotFoundException("No ID was passed");
        }
        productRepository.deleteAllById(ids);
    }


    public List<ProductEntity> findProducts(ProductQuery productToFind) {
        return productRepository.findProducts(productToFind);

    }
}
