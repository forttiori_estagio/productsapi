package com.bruno.productsapi.service;

import com.bruno.productsapi.integration.mercadolivrewebscrapper.MercadoLivreWebScrapperService;
import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.ShippingRequest;
import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.model.entity.ProductQuery;
import com.bruno.productsapi.model.mapper.request.ProductRequestMapper;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import com.bruno.productsapi.integration.shipping.ShippingService;
import com.bruno.productsapi.model.mapper.request.ProductRequest;
import com.bruno.productsapi.model.mapper.response.ProductResponseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ProductServiceFacade {

    private final ProductService productService;

    private final ShippingService shippingService;

    private final MercadoLivreWebScrapperService mercadoLivreWebScrapperService;

    public List<ProductResponse> getProducts(List<String> ids) {
       return productService.getProducts(ids).stream().map(ProductResponseMapper::entityToResponse).toList();
    }

    public Page<ProductResponse> getProductsPageable(Pageable pageable) {
        return productService.getProductsPageable(pageable).map(ProductResponseMapper::entityToResponse);
    }

    public ProductResponse addProduct(ProductRequest productRequest) {
        Double productPrice = mercadoLivreWebScrapperService.fetchProductPrice(productRequest.getProductName());
        return ProductResponseMapper.entityToResponse
                (productService.addProduct(ProductRequestMapper.requestToEntity(productRequest, productPrice)));
    }

    public ProductResponse updateProduct(ProductRequest productRequest, String id) {
        Double productPrice = mercadoLivreWebScrapperService.fetchProductPrice(productRequest.getProductName());
        return ProductResponseMapper.entityToResponse(
                productService.updateProduct(ProductRequestMapper.requestToEntity(productRequest, productPrice), id));
    }

    public void deleteProducts(List<String> ids, Boolean deleteAll) {
        productService.deleteProducts(ids, deleteAll);
    }

    public List<ShippingEntity> calculateShipping(ShippingRequest shippingRequest, String id) {
        return shippingService.calculateShipping(shippingRequest, id);
    }

    public List<ProductResponse> findProducts(ProductQuery productToFind) {
        return productService.findProducts(productToFind).stream().map(ProductResponseMapper::entityToResponse).toList();
    }
}
