package com.bruno.productsapi.configurations.annotations;

import com.bruno.productsapi.exceptions.customexceptions.StandardErrorResponse;
import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ApiOperation(value = "Calculates the Shipping Cost", notes = "Returns the shipping cost, deadline and delivery company " +
        "by passing a product ID along with an Origin and Destination CEP", response = ShippingEntity.class)
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully calculated."),
        @ApiResponse(code = 400, message = "No response from the Shipping API.", response = StandardErrorResponse.class)
}
)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SwaggerShippingInit {


}
