package com.bruno.productsapi.configurations.annotations;

import com.bruno.productsapi.exceptions.customexceptions.StandardErrorResponse;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ApiOperation(value = "Adds a new Product", notes = "Adds a new product to the Database and returns it.", response = ProductResponse.class)
@ApiResponses(value = {
        @ApiResponse(code = 201, message = "Success"),
        @ApiResponse(code = 400, message = "Bad request.", response = StandardErrorResponse.class),
})

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SwaggerPostInit {

}


