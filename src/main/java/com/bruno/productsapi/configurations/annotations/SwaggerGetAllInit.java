package com.bruno.productsapi.configurations.annotations;

import com.bruno.productsapi.exceptions.customexceptions.StandardErrorResponse;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ApiOperation(value = "Get All or Get By ID", notes = "Get All Products in the Database, " +
        "or a list of Products by passing the ID's as request parameters.", response = ProductResponse.class)
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success"),
        @ApiResponse(code = 400, message = "Bad Request", response = StandardErrorResponse.class)
})
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SwaggerGetAllInit {

}
