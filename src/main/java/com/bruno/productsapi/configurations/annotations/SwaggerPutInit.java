package com.bruno.productsapi.configurations.annotations;

import com.bruno.productsapi.exceptions.customexceptions.StandardErrorResponse;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ApiOperation(value = "Update a Product", notes = "Updates a product in the Database.", response = ProductResponse.class)
@ApiResponses(value = {
        @ApiResponse(code = 201, message = "Product Updated Successfully."),
        @ApiResponse(code = 400, message = "Bad request. Invalid ID and/or input sent for one or more attributes.", response = StandardErrorResponse.class),
})
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SwaggerPutInit {


}
