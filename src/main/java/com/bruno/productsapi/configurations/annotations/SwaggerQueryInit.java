package com.bruno.productsapi.configurations.annotations;

import com.bruno.productsapi.exceptions.customexceptions.StandardErrorResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ApiOperation(value = "Query for Products", notes = "Filter all the Products by Name," +
        " category and price.")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success."),
        @ApiResponse(code = 400, message = "Bad request.", response = StandardErrorResponse.class)
})
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SwaggerQueryInit {
}
