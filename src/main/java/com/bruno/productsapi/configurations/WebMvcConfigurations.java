package com.bruno.productsapi.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class WebMvcConfigurations implements WebMvcConfigurer {


    // Configuring the Pageable - Number of Items per Page, Default Page
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        PageableHandlerMethodArgumentResolver pageHandler = new PageableHandlerMethodArgumentResolver();
        pageHandler.setFallbackPageable(PageRequest.of(0, 50));
        resolvers.add(pageHandler);
    }

    // Configuring the CORS access globally
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/v1/products").allowedOrigins("http://localhost");
    }

}
