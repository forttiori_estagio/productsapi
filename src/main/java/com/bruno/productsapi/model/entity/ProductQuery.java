package com.bruno.productsapi.model.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("ALL")
@Data // Lombok's annotation for creating Getters, Setters, Equals, HashCode and To String
@Builder // Lombok's annotation for building objects easily
@AllArgsConstructor // Constructor with all attributes using Lombok
@NoArgsConstructor // Empty Constructor using Lombok
public class ProductQuery {

    @Builder.Default
    @ApiModelProperty(position = 0,
            value = "Passed Empty when the name is not being queried",
            example = "Action Figure Kakashi")
    private String productName = "";
    @Builder.Default
    @ApiModelProperty(position = 1,
            value = "Passed 'any' when the category is not being queried",
            example = "vestuario")
    private Categories productCategory = Categories.ANY;
    @Builder.Default
    @ApiModelProperty(position = 2,
            value = "Omit this attribute when price floor is not being queried.",
            example = "250.00")
    private Double priceFloor = 0.00;
    @Builder.Default
    @ApiModelProperty(position = 3,
            value = "Omit this attribute when price ceil is not being queried.",
            example = "500.00")
    private Double priceCeil = 9999.99;

}
