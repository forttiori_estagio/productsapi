package com.bruno.productsapi.model.mapper.response;

import com.bruno.productsapi.model.entity.ProductEntity;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ProductResponseMapper {

    public static ProductResponse entityToResponse(ProductEntity productEntity) {

        return ProductResponse.builder()
                .id(productEntity.getId())
                .productName(productEntity.getProductName())
                .productCategory(productEntity.getProductCategory())
                .productPrice(productEntity.getProductPrice())
                .productWeight(productEntity.getProductWeight())
                .productHeight(productEntity.getProductHeight())
                .productWidth(productEntity.getProductWidth())
                .productLength(productEntity.getProductLength())
                .updatedAt(productEntity.getUpdatedAt())
                .build();
    }

}
