package com.bruno.productsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("ALL")
@SpringBootApplication
public class ProductsapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsapiApplication.class, args);
	}

}
