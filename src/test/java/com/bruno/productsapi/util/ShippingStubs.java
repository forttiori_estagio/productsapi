package com.bruno.productsapi.util;

import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.From;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.Package;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.ShippingRequest;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.To;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.Company;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.ShippingResponse;

import java.util.List;

public class ShippingStubs {

    public static ShippingEntity[] createArrayOfShippingEntities() {
        return new ShippingEntity[]{
                ShippingEntity.builder()
                        .shippingPrice(1.0)
                        .deliveryName("Test Delivery Name")
                        .deliveryTime(1)
                        .company("Test Company")
                        .build()
        };

    }

    public static ShippingRequest createShippingRequestWithPackage() {
        return ShippingRequest.builder()
                .from(From.builder().postalCode("91030320").build())
                .to(To.builder().postalCode("95625000").build())
                .packageInfo(Package.builder()
                        .height(1)
                        .weight(1.0)
                        .length(1)
                        .width(1)
                        .build())
                .build();
    }

    public static ShippingRequest createShippingRequestWithoutPackage() {
        return ShippingRequest.builder()
                .from(From.builder().postalCode("91030320").build())
                .to(To.builder().postalCode("95625000").build())
                .build();
    }

    public static ShippingRequest createInvalidShippingRequest() {
        return ShippingRequest.builder()
                .from(From.builder().postalCode("910").build())
                .to(To.builder().postalCode("95625000").build())
                .build();
    }

    public static List<ShippingResponse> createListOfShippingResponses() {
        return List.of(ShippingResponse.builder()
                        .id(1)
                        .company(Company.builder().id(1).name("Test Company").picture("Test picture").build())
                        .price("1")
                        .name("Test Delivery Name")
                        .deliveryTime(1)
                        .build(),
                ShippingResponse.builder()
                        .id(1)
                        .company(Company.builder().id(1).name("Test Company").picture("Test picture").build())
                        .price("1")
                        .name("Test Delivery Name")
                        .deliveryTime(1)
                        .build());
    }

    public static ShippingResponse createShippingResponse() {
        return ShippingResponse.builder()
                .id(1)
                .company(Company.builder().id(1).name("Test Company").picture("Test picture").build())
                .price("1")
                .name("Test Delivery Name")
                .deliveryTime(1)
                .build();
    }

    public static ShippingResponse createShippingResponseWithError() {
        return ShippingResponse.builder()
                .id(1)
                .company(Company.builder().id(1).name("Test Company").picture("Test picture").build())
                .price("1")
                .name("Test Delivery Name")
                .deliveryTime(1)
                .error("Test Error")
                .build();
    }
}
