package com.bruno.productsapi.util;

import com.bruno.productsapi.model.entity.Categories;
import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.model.entity.ProductQuery;
import com.bruno.productsapi.model.mapper.request.ProductRequest;
import com.bruno.productsapi.model.mapper.response.ProductResponse;

import java.time.LocalDateTime;

public class ProductStubs {

    public static ProductRequest createProductRequestStub() {
        return ProductRequest.builder()
                .productName("Product Test")
                .productCategory(Categories.CANECAS)
                .productHeight(1)
                .productWidth(1)
                .productLength(1)
                .productWeight(1.0)
                .build();
    }

    public static  ProductRequest createInvalidProductRequestStub() {
        return ProductRequest.builder()
                .productName("Invalid Product Test")
                .productCategory(Categories.CANECAS)
                .productHeight(1)
                .productWidth(1)
                .productLength(1)
                .productWeight(1.0)
                .build();
    }

    public static ProductResponse createProductResponseStub() {
        return ProductResponse.builder()
                .id("1")
                .productName("Product Test")
                .productCategory(Categories.CANECAS)
                .productPrice(0.01)
                .productHeight(1)
                .productWidth(1)
                .productLength(1)
                .productWeight(1.0)
                .build();
    }
    public static ProductEntity createProductEntityStub() {
        return ProductEntity.builder()
                .id("1")
                .productName("Product Test")
                .productCategory(Categories.CANECAS)
                .productPrice(0.01)
                .inventory(1)
                .productHeight(1)
                .productWeight(1.0)
                .productWidth(1)
                .productLength(1)
                .updatedAt(LocalDateTime.now())
                .build();
    }

    public static ProductEntity createProductEntityStub1() {
        return ProductEntity.builder()
                .id("1")
                .productName("Product Test 1")
                .productCategory(Categories.CANECAS)
                .productPrice(0.01)
                .inventory(1)
                .productHeight(1)
                .productWeight(1.0)
                .productWidth(1)
                .productLength(1)
                .updatedAt(LocalDateTime.now())
                .build();
    }
    public static ProductEntity createProductEntityStub2() {
        return ProductEntity.builder()
                .id("2")
                .productName("Product Test 2")
                .productCategory(Categories.CANECAS)
                .productPrice(0.01)
                .inventory(1)
                .productHeight(1)
                .productWeight(1.0)
                .productWidth(1)
                .productLength(1)
                .updatedAt(LocalDateTime.now())
                .build();
    }
    public static ProductEntity createProductEntityStub3() {
        return ProductEntity.builder()
                .id("3")
                .productName("Product Test 3")
                .productCategory(Categories.CANECAS)
                .productPrice(2.0)
                .inventory(1)
                .productHeight(1)
                .productWeight(1.0)
                .productWidth(1)
                .productLength(1)
                .updatedAt(LocalDateTime.now())
                .build();
    }
    public static  ProductEntity createInvalidProductEntityStub() {
        return ProductEntity.builder()
                .productName("Invalid Product Test")
                .productCategory(Categories.CANECAS)
                .productPrice(-10.0)
                .productHeight(1)
                .productWidth(1)
                .productLength(1)
                .productWeight(1.0)
                .build();
    }

    public static ProductQuery createValidProductQuery() {
        return ProductQuery.builder()
                .productName("Product Test 1")
                .productCategory(Categories.ANY)
                .priceCeil(1.0)
                .priceFloor(0.0)
                .build();
    }

}
