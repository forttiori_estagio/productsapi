package com.bruno.productsapi.integration;

import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.ShippingClient;
import com.bruno.productsapi.model.entity.Categories;
import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.model.entity.ProductQuery;
import com.bruno.productsapi.model.mapper.request.ProductRequest;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import com.bruno.productsapi.repository.ProductRepository;
import com.bruno.productsapi.util.ProductStubs;
import com.bruno.productsapi.util.ShippingStubs;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    ProductRepository productRepository;

    @MockBean
    ShippingClient shippingClient;

    // Populate the Database With Three Product Entities Before All The Tests
    @BeforeEach
    public void setup() {
        List<ProductEntity> productEntityStubs = List.of(
                ProductStubs.createProductEntityStub1(),
                ProductStubs.createProductEntityStub2(),
                ProductStubs.createProductEntityStub3());
        productRepository.saveAll(productEntityStubs);
    }

    // Drop Database After All The Tests Are Executed
    @AfterEach
    public void clearUp() {
        productRepository.deleteAll();
    }

    @Test
    @DisplayName("Calculate Shipping Should Throw Validation Exception When One of the CEP codes is Invalid.")
    void calculateShipping_ShouldThrowValidationException_WhenCepCodeInvalid() throws Exception {
        // When
        MvcResult mvcResult = mockMvc.perform(get("/v1/products/shipping/{id}", "1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(ShippingStubs.createInvalidShippingRequest())))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        String json = mvcResult.getResponse().getContentAsString();

        // Then
        Assertions.assertThat(json).contains("Validation Exception");
    }

    @Test
    @DisplayName("Calculate Shipping Should Return List Of Shipping Entities When ID and CEP Codes Are Valid")
    void calculateShipping_ShouldReturnListOfShippingEntities_WhenCepCodesAndIdAreValid() throws Exception {
        // Given
        Mockito.when(shippingClient.fetchShippingData(ShippingStubs.createShippingRequestWithPackage()))
                .thenReturn(List.of(ShippingStubs.createShippingResponse()));

        ShippingEntity[] expectedEntities = ShippingStubs.createArrayOfShippingEntities();

        // When
        MvcResult mvcResult = mockMvc.perform(get("/v1/products/shipping/{id}", "1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(ShippingStubs.createShippingRequestWithoutPackage())))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String json = mvcResult.getResponse().getContentAsString();
        ShippingEntity[] shippingEntities = objectMapper.readValue(json, ShippingEntity[].class);

        // Then
        Assertions.assertThat(shippingEntities).isNotEmpty().isEqualTo(expectedEntities);
    }

    @Test
    @DisplayName("Calculate Shipping Should Throw Entity Not Found Exception When ID is Invalid.")
    void calculateShipping_ShouldThrowEntityNotFoundException_WhenIdIsInvalid() throws Exception {
        // When
        MvcResult mvcResult = mockMvc.perform(get("/v1/products/shipping/{id}", "invalid")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(ShippingStubs.createShippingRequestWithoutPackage())))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();

        String json = mvcResult.getResponse().getContentAsString();

        // Then
        Assertions.assertThat(json).contains("Not Found Exception").contains("ID not found = invalid");
    }

    @Test
    @DisplayName("Get All Products returns a list of Product Responses when no ID's are passed.")
    void getAllProducts_ShouldReturnAListOfProductResponses_WhenNoIdsArePassed() throws Exception {
        // When
        MvcResult mvcResult = mockMvc.perform(get("/v1/products/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String json = mvcResult.getResponse().getContentAsString();
        ProductResponse[] productResponses = objectMapper.readValue(json, ProductResponse[].class);

        // Then
        Assertions.assertThat(productResponses)
                .isInstanceOf(ProductResponse[].class)
                .hasSize(3);
    }

    @Test
    @DisplayName("Get All Products returns a List with a Single Product Response When a Valid ID is passed.")
    void getAllProducts_ShouldReturnAListWithASingleProductResponse_WhenAValidIdIsPassed() throws Exception {
        // When
        MvcResult mvcResult = mockMvc.perform(get("/v1/products/?ids=1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String json = mvcResult.getResponse().getContentAsString();
        ProductResponse[] productResponses = objectMapper.readValue(json, ProductResponse[].class);

        // Then
        Assertions.assertThat(productResponses)
                .isInstanceOf(ProductResponse[].class)
                .hasSize(1);
    }

    @Test
    @DisplayName("Get All Products throws Entity Not Found Exception When ID passed is Invalid")
    void getAllProducts_ShouldThrowEntityNotFoundException_WhenInvalidIdIsPassed() throws Exception {

        // When
        MvcResult mvcResult = mockMvc.perform(get("/v1/products/?ids=invalid"))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();

        // Then

        Assertions.assertThat(jsonResponse).contains("Not Found Exception");
    }

    @Test
    @DisplayName("Get All Pageable returns a Page of Product Responses")
    void getAllPageable_ShouldReturnAPageObject_WhenSuccessful() throws Exception {
        // When
        MvcResult mvcResult = mockMvc.perform(get("/v1/products/pageable"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // Then
        Assertions.assertThat(mvcResult.getResponse().getContentAsString())
                .isNotEmpty()
                .isNotNull()
                .contains("content");
    }

    @Test
    @DisplayName("Find Products Should Return A List Of Products When Request Body is Valid With Any Category")
    void findProducts_ShouldReturnAListOfProductResponses_WhenValidRequestBodyIsPassedWithAnyCategory() throws Exception {
        //Given
        ProductQuery validProductQuery = ProductStubs.createValidProductQuery();

        // When
        MvcResult mvcResult = mockMvc.perform(get("/v1/products/query")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper
                                .writeValueAsString(validProductQuery)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String json = mvcResult.getResponse().getContentAsString();
        ProductResponse[] productResponses = objectMapper.readValue(json, ProductResponse[].class);

        // Then
        Assertions.assertThat(productResponses)
                .isInstanceOf(ProductResponse[].class)
                .hasSize(1);

        Assertions.assertThat(Arrays.stream(productResponses)
                        .toList().get(0).getProductName())
                .isEqualTo(validProductQuery.getProductName());
    }

    @Test
    @DisplayName("Find Products Should Return A List Of Products When Request Body is Valid With Specific Category")
    void findProducts_ShouldReturnAListOfProductResponses_WhenValidRequestBodyIsPassedWithSpecificCategory() throws Exception {
        //Given
        ProductQuery validProductQuery = ProductStubs.createValidProductQuery();
        validProductQuery.setProductName("");
        validProductQuery.setProductCategory(Categories.CANECAS);

        // When
        MvcResult mvcResult = mockMvc.perform(get("/v1/products/query")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper
                                .writeValueAsString(validProductQuery)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String json = mvcResult.getResponse().getContentAsString();
        ProductResponse[] productResponses = objectMapper.readValue(json, ProductResponse[].class);

        // Then
        Assertions.assertThat(productResponses)
                .isInstanceOf(ProductResponse[].class)
                .hasSize(2);
    }

    @Test
    @DisplayName("Add Product Should Persist a Product in the Database and Return Given Product")
    void addProduct_ShouldPersistProductAndReturnIt_WhenAValidProductRequestIsPassed() throws Exception {
        // Given
        ProductRequest productToAdd = ProductStubs.createProductRequestStub();

        // When
        MvcResult mvcResult = mockMvc.perform(post("/v1/products/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productToAdd)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();

        String json = mvcResult.getResponse().getContentAsString();
        ProductResponse productResponse = objectMapper.readValue(json, ProductResponse.class);

        // Then
        Assertions.assertThat(productResponse)
                .isInstanceOf(ProductResponse.class);


        Assertions.assertThat(productResponse
                        .getProductName())
                .isEqualTo(productToAdd.getProductName());
    }

    @Test
    @DisplayName("Add Product Should Throw a Validation Exception when an Invalid Product is Passed")
    void addProduct_ShouldThrowAValidationException_WhenAnInvalidProductIsPassed() throws Exception {
        // Given
        ProductRequest productToAdd = ProductStubs.createInvalidProductRequestStub();

        // When
        MvcResult mvcResult = mockMvc.perform(post("/v1/products/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productToAdd)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();

        // Then
        Assertions.assertThat(jsonResponse).contains("Validation Exception").contains("Product Price must not be negative.");
    }

    @Test
    @DisplayName("Update Product Should Persist an Updated Product in the Database and Return it")
    void updateProduct_ShouldPersistUpdatedProductAndReturnIt_WhenAValidProductRequestAndIdIsPassed() throws Exception {
        // Given
        ProductRequest productToUpdate = ProductStubs.createProductRequestStub();
        productToUpdate.setProductName("Updated Test Product");

        // When
        MvcResult mvcResult = mockMvc.perform(put("/v1/products/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productToUpdate)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();

        String json = mvcResult.getResponse().getContentAsString();
        ProductResponse productResponse = objectMapper.readValue(json, ProductResponse.class);

        // Then
        Assertions.assertThat(productResponse)
                .isInstanceOf(ProductResponse.class);

        Assertions.assertThat(productResponse
                        .getProductName())
                .isEqualTo(productToUpdate.getProductName());
    }

    @Test
    @DisplayName("Update Product Should Throw Entity Not Found Exception when an Invalid ID is passed.")
    void updateProduct_ShouldThrowEntityNotFoundException_WhenAnInvalidIdIsPassed() throws Exception {
        // Given
        ProductRequest productToUpdate = ProductStubs.createProductRequestStub();
        productToUpdate.setProductName("Updated Test Product");

        // When
        MvcResult mvcResult = mockMvc.perform(put("/v1/products/{id}", "invalid")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productToUpdate)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();

        // Then
        Assertions.assertThat(jsonResponse).contains("Not Found Exception").contains("ID not found = invalid");
    }

    @Test
    @DisplayName("Delete Product Should Delete Given Product From the Database")
    void deleteProducts_ShouldDeleteOneOrMoreProductsFromTheDatabase_WhenValidIdsArePassed() throws Exception {
        // When
        mockMvc.perform(delete("/v1/products/?ids=1&deleteAll=false"))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andReturn();

        // Then
        Assertions.assertThat(productRepository.findAll())
                .hasSize(2);

        Assertions.assertThat(productRepository.findById("1")).isNotPresent();
    }

    @Test
    @DisplayName("Delete Product Should Throw Entity Not Found Exception When No IDs are Passed")
    void deleteProducts_ShouldThrowEntityNotFoundException_WhenNoIdsArePassed() throws Exception {
        // When
        MvcResult mvcResult = mockMvc.perform(delete("/v1/products/?deleteAll=false"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();

        // Then
        Assertions.assertThat(jsonResponse).contains("Not Found Exception").contains("No ID was passed");
    }

    @Test
    @DisplayName("Delete Product Should Clear Database if DeleteAll is Passed as True")
    void deleteProducts_ShouldDeleteEntireDatabase_WhenDeleteAllIsPassedAsTrue() throws Exception {
        // When
        mockMvc.perform(delete("/v1/products/?deleteAll=true"))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andReturn();

        // Then
        Assertions.assertThat(productRepository.findAll())
                .isEmpty();
    }
}

